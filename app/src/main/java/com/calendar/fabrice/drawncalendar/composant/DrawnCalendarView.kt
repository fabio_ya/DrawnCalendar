package com.calendar.fabrice.drawncalendar.composant

import android.content.Context
import android.graphics.Typeface
import android.support.v4.content.ContextCompat
import android.support.v4.graphics.drawable.DrawableCompat
import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.calendar.fabrice.drawncalendar.R
import kotlinx.android.synthetic.main.drawn_calendar_month_layout.view.*
import java.text.DateFormatSymbols
import java.util.*

/**
 * Created by fabrice on 19/08/2017.
 */



class DrawnCalendarView : LinearLayout{

    companion object {

        private val DAY_OF_THE_WEEK_TEXT = "dayOfTheWeekText"
        private val DAY_OF_THE_WEEK_LAYOUT = "dayOfTheWeekLayout"

        private val DAY_OF_THE_MONTH_LAYOUT = "dayOfTheMonthLayout"
        private val DAY_OF_THE_MONTH_TEXT = "dayOfTheMonthText"
        private val DAY_OF_THE_MONTH_BACKGROUND = "dayOfTheMonthBackground"
        private val DAY_OF_THE_MONTH_CIRCLE_IMAGE_1 = "dayOfTheMonthCircleImage1"
        private val DAY_OF_THE_MONTH_CIRCLE_IMAGE_2 = "dayOfTheMonthCircleImage2"
    }

    // View
    private var cxt: Context? = null

    // Class
    private var drawnCalendarListener: DrawnCalendarListener? = null
    private var currentCalendar: Calendar? = null
    private var lastSelectedDayCalendar: Calendar? = null

    private var shortWeekDays = false


    constructor(ctx : Context) : super(ctx){
        this.cxt = cxt
        onCreateView()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        this.cxt = context
        onCreateView()
        val ta = context.obtainStyledAttributes(attrs, R.styleable.DrawnCalendarView, 0, 0)
        try {
            val showCalendarBar = ta.getBoolean(R.styleable.DrawnCalendarView_showDateTitle, true);
            showDateTitle(showCalendarBar)
        } finally {
            ta.recycle();
        }
    }

    private fun onCreateView(): View {
        val inflate = cxt!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        inflate.inflate(R.layout.drawn_calendar_picker_layout, this, true)
        findViewsById()
        setUpEventListeners()
        setUpCalligraphy()
        return rootView
    }

    private fun findViewsById() {


        for (i in 0..41) {

            val inflates = cxt?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val weekIndex = i % 7 + 1
            val dayOfTheWeekLayout = rootView.findViewWithTag<ViewGroup>(DAY_OF_THE_WEEK_LAYOUT + weekIndex)

            // Create day of the month
            val dayOfTheMonthLayout = inflates.inflate(R.layout.drawn_calendar_day_of_the_month_layout, null)
            val dayOfTheMonthText = dayOfTheMonthLayout.findViewWithTag<View>(DAY_OF_THE_MONTH_TEXT)
            val dayOfTheMonthBackground = dayOfTheMonthLayout.findViewWithTag<View>(DAY_OF_THE_MONTH_BACKGROUND)
            val dayOfTheMonthCircleImage1 = dayOfTheMonthLayout.findViewWithTag<View>(DAY_OF_THE_MONTH_CIRCLE_IMAGE_1)
            val dayOfTheMonthCircleImage2 = dayOfTheMonthLayout.findViewWithTag<View>(DAY_OF_THE_MONTH_CIRCLE_IMAGE_2)

            // Set tags to identify them
            val viewIndex = i + 1
            dayOfTheMonthLayout.setTag(DAY_OF_THE_MONTH_LAYOUT + viewIndex)
            dayOfTheMonthText.setTag(DAY_OF_THE_MONTH_TEXT + viewIndex)
            dayOfTheMonthBackground.setTag(DAY_OF_THE_MONTH_BACKGROUND + viewIndex)
            dayOfTheMonthCircleImage1.setTag(DAY_OF_THE_MONTH_CIRCLE_IMAGE_1 + viewIndex)
            dayOfTheMonthCircleImage2.setTag(DAY_OF_THE_MONTH_CIRCLE_IMAGE_2 + viewIndex)

            dayOfTheWeekLayout.addView(dayOfTheMonthLayout)

        }

    }

    private fun setUpEventListeners() {

        leftButton.setOnClickListener {
            if (drawnCalendarListener == null) {
                throw IllegalStateException("You must assign a valid RobotoCalendarListener first!")
            }

            // Decrease month
            currentCalendar?.add(Calendar.MONTH, -1)
            lastSelectedDayCalendar = null
            updateView()
            drawnCalendarListener!!.onLeftButtonClick()
        }

        rightButton.setOnClickListener {
            if (drawnCalendarListener == null) {
                throw IllegalStateException("You must assign a valid RobotoCalendarListener first!")
            }

            // Increase month
            currentCalendar?.add(Calendar.MONTH, 1)
            lastSelectedDayCalendar = null
            updateView()
            drawnCalendarListener?.onRightButtonClick()
        }
    }

    private fun setUpCalligraphy() {
        // Initialize calendar for current month
        val currentCalendar = Calendar.getInstance()
        setCalendar(currentCalendar)
    }

    // ************************************************************************************************************************************************************************
    // * Auxiliary UI methods
    // ************************************************************************************************************************************************************************

    private fun setUpMonthLayout() {
        var dateText = DateFormatSymbols(Locale.getDefault()).months[currentCalendar!!.get(Calendar.MONTH)]
        dateText = dateText.substring(0, 1).toUpperCase() + dateText.subSequence(1, dateText.length)
        val calendar = Calendar.getInstance()
        if (currentCalendar?.get(Calendar.YEAR) == calendar.get(Calendar.YEAR)) {
            monthText.text = dateText
        } else {
            monthText.text = String.format("%s %s", dateText, currentCalendar?.get(Calendar.YEAR))
        }
    }

    private fun setUpWeekDaysLayout() {
        var dayOfWeek: TextView
        var dayOfTheWeekString: String
        val weekDaysArray = DateFormatSymbols(Locale.getDefault()).weekdays
        for (i in 1..weekDaysArray.size - 1) {
            dayOfWeek = rootView.findViewWithTag(DAY_OF_THE_WEEK_TEXT + getWeekIndex(i, currentCalendar))
            dayOfTheWeekString = weekDaysArray[i]
            if (shortWeekDays) {
                dayOfTheWeekString = checkSpecificLocales(dayOfTheWeekString, i)
            } else {
                dayOfTheWeekString = dayOfTheWeekString.substring(0, 1).toUpperCase() + dayOfTheWeekString.substring(1, 3)
            }

            dayOfWeek.text = dayOfTheWeekString
        }
    }
    private fun setUpDaysOfMonthLayout() {

        var dayOfTheMonthText: TextView
        var circleImage1: View
        var circleImage2: View
        var dayOfTheMonthContainer: ViewGroup
        var dayOfTheMonthBackground: ViewGroup

        for (i in 1..42) {

            dayOfTheMonthContainer = rootView.findViewWithTag<ViewGroup>(DAY_OF_THE_MONTH_LAYOUT + i)
            dayOfTheMonthBackground = rootView.findViewWithTag<ViewGroup>(DAY_OF_THE_MONTH_BACKGROUND + i)
            dayOfTheMonthText = rootView.findViewWithTag<TextView>(DAY_OF_THE_MONTH_TEXT + i)
            circleImage1 = rootView.findViewWithTag(DAY_OF_THE_MONTH_CIRCLE_IMAGE_1 + i)
            circleImage2 = rootView.findViewWithTag(DAY_OF_THE_MONTH_CIRCLE_IMAGE_2 + i)

            dayOfTheMonthText.visibility = View.INVISIBLE
            circleImage1.visibility = View.GONE
            circleImage2.visibility = View.GONE

            // Apply styles
            dayOfTheMonthText.setBackgroundResource(android.R.color.transparent)
            dayOfTheMonthText.setTypeface(null, Typeface.NORMAL)
            dayOfTheMonthText.setTextColor(ContextCompat.getColor(cxt, R.color.drawn_calendar_day_of_the_month_font))
            dayOfTheMonthContainer.setBackgroundResource(android.R.color.transparent)
            dayOfTheMonthContainer.setOnClickListener(null)
            dayOfTheMonthBackground.setBackgroundResource(android.R.color.transparent)
        }
    }

    private fun setUpDaysInCalendar() {

        val auxCalendar = Calendar.getInstance(Locale.getDefault())
        auxCalendar.time = currentCalendar?.time
        auxCalendar.set(Calendar.DAY_OF_MONTH, 1)
        val firstDayOfMonth = auxCalendar.get(Calendar.DAY_OF_WEEK)
        var dayOfTheMonthText: TextView?
        var dayOfTheMonthContainer: ViewGroup
        var dayOfTheMonthLayout: ViewGroup

        // Calculate dayOfTheMonthIndex
        var dayOfTheMonthIndex = getWeekIndex(firstDayOfMonth, auxCalendar)

        run {
            var i = 1
            while (i <= auxCalendar.getActualMaximum(Calendar.DAY_OF_MONTH)) {
                dayOfTheMonthContainer = rootView.findViewWithTag(DAY_OF_THE_MONTH_LAYOUT + dayOfTheMonthIndex)
                dayOfTheMonthText = rootView.findViewWithTag(DAY_OF_THE_MONTH_TEXT + dayOfTheMonthIndex)
                if (dayOfTheMonthText == null) {
                    break
                }
                dayOfTheMonthContainer.setOnClickListener(onDayOfMonthClickListener)
                dayOfTheMonthContainer.setOnLongClickListener(onDayOfMonthLongClickListener)
                dayOfTheMonthText?.visibility = View.VISIBLE
                dayOfTheMonthText?.text = i.toString()
                i++
                dayOfTheMonthIndex++
            }
        }

        for (i in 36..42) {
            dayOfTheMonthText = rootView.findViewWithTag(DAY_OF_THE_MONTH_TEXT + i)
            dayOfTheMonthLayout = rootView.findViewWithTag(DAY_OF_THE_MONTH_LAYOUT + i)
            if (dayOfTheMonthText?.visibility == View.INVISIBLE) {
                dayOfTheMonthLayout.visibility = View.GONE
            } else {
                dayOfTheMonthLayout.visibility = View.VISIBLE
            }
        }
    }

    private fun markDayAsCurrentDay() {
        // If it's the current month, mark current day
        val nowCalendar = Calendar.getInstance()
        if (nowCalendar.get(Calendar.YEAR) == currentCalendar!!.get(Calendar.YEAR) && nowCalendar.get(Calendar.MONTH) == currentCalendar!!.get(Calendar.MONTH)) {
            val currentCalendar = Calendar.getInstance()
            currentCalendar.time = nowCalendar.time

            val dayOfTheMonthBackground = getDayOfMonthBackground(currentCalendar)
            // choose to doesnt rounded the current date
            dayOfTheMonthBackground.setBackgroundResource(R.drawable.ring);
        }
    }

    fun clearSelectedDay(calendar: Calendar?, circle1: Boolean?, circle2: Boolean?) {
        if (calendar != null) {

            val dayOfTheMonthBackground = getDayOfMonthBackground(calendar)

            val nowCalendar = calendar

            val dayOfTheMonth = getDayOfMonthText(calendar)
            dayOfTheMonth.setTextColor(ContextCompat.getColor(cxt, R.color.drawn_calendar_day_of_the_month_font))

            val circleImage1 = getCircleImage1(calendar)
            val circleImage2 = getCircleImage2(calendar)
            if (circleImage1.visibility == View.VISIBLE && circle1!!) {
                Log.e("CLEAR SELECT DAY", "MOORNING")
                circleImage1.visibility = View.INVISIBLE
                DrawableCompat.setTint(circleImage1.drawable, ContextCompat.getColor(cxt, R.color.drawn_calendar_background))
            }

            if (circleImage2.visibility == View.VISIBLE && circle2!!) {
                Log.e("CLEAR SELECT DAY", "AFTERNOON")
                circleImage2.visibility = View.INVISIBLE
                DrawableCompat.setTint(circleImage2.drawable, ContextCompat.getColor(cxt, R.color.drawn_calendar_background))
            }
        }
    }

    private fun checkSpecificLocales(dayOfTheWeekString: String, i: Int): String {
        var dayOfTheWeekString = dayOfTheWeekString
        // Set Wednesday as "X" in Spanish Locale.getDefault()
        if (i == 4 && Locale.getDefault().country == "ES") {
            dayOfTheWeekString = "X"
        } else {
            dayOfTheWeekString = dayOfTheWeekString.substring(0, 1).toUpperCase()
        }
        return dayOfTheWeekString
    }

    // ************************************************************************************************************************************************************************
    // * Public calendar methods
    // ************************************************************************************************************************************************************************

    /**
     * Set an specific calendar to the view

     * @param calendar
     */
    fun setCalendar(calendar: Calendar) {
        this.currentCalendar = calendar
        updateView()
    }

    /**
     * Update the calendar view
     */
    fun updateView() {
        setUpMonthLayout()
        setUpWeekDaysLayout()
        setUpDaysOfMonthLayout()
        setUpDaysInCalendar()
        markDayAsCurrentDay()
    }

    fun setShortWeekDays(shortWeekDays: Boolean) {
        this.shortWeekDays = shortWeekDays
    }

    /**
     * Clear the view of marks and selections
     */
    fun clearCalendar() {
        updateView()
    }

    fun markCircleImage1(calendar: Calendar) {
        val circleImage1 = getCircleImage1(calendar)
        circleImage1.visibility = View.VISIBLE
        DrawableCompat.setTint(circleImage1.drawable, ContextCompat.getColor(cxt, R.color.drawn_calendar_circle_1))
    }

    fun markCircleImage2(calendar: Calendar) {
        val circleImage2 = getCircleImage2(calendar)
        circleImage2.visibility = View.VISIBLE
        DrawableCompat.setTint(circleImage2.drawable, ContextCompat.getColor(cxt, R.color.drawn_calendar_circle_2))
    }

    fun markDayUnClickable(calendar: Calendar) {
        val v = getDayOfMonthBackground(calendar)
        val tv = getDayOfMonthText(calendar)
        tv.setTextColor(ContextCompat.getColor(cxt, R.color.text_color))
        tv.background = ContextCompat.getDrawable(cxt, R.drawable.circle_grey)
    }


    fun showDateTitle(show: Boolean) {
        if (show) {
            DrawnCalendarDateTitleContainer.visibility = View.VISIBLE
        } else {
            DrawnCalendarDateTitleContainer.visibility = View.GONE
        }
    }

    // ************************************************************************************************************************************************************************
    // * Public interface
    // ************************************************************************************************************************************************************************

    interface DrawnCalendarListener {

        fun onDayClick(daySelectedCalendar: Calendar)

        fun onDayLongClick(daySelectedCalendar: Calendar)

        fun onRightButtonClick()

        fun onLeftButtonClick()
    }

    fun setDrawnCalendarListener(drawnCalendarListener: DrawnCalendarListener) {
        this.drawnCalendarListener = drawnCalendarListener
    }

    // ************************************************************************************************************************************************************************
    // * Event handler methods
    // ************************************************************************************************************************************************************************

    private val onDayOfMonthClickListener = View.OnClickListener { view ->
        // Extract day selected
        val dayOfTheMonthContainer = view as ViewGroup
        var tagId = dayOfTheMonthContainer.tag as String
        tagId = tagId.substring(DAY_OF_THE_MONTH_LAYOUT.length, tagId.length)
        val dayOfTheMonthText = view.findViewWithTag<TextView>(DAY_OF_THE_MONTH_TEXT + tagId)

        // Extract the day from the text
        val calendar = Calendar.getInstance()
        currentCalendar?.get(Calendar.YEAR)?.let { calendar.set(Calendar.YEAR, it) }
        currentCalendar?.get(Calendar.MONTH)?.let { calendar.set(Calendar.MONTH, it) }
        calendar.set(Calendar.DAY_OF_MONTH, Integer.valueOf(dayOfTheMonthText.text.toString())!!)

        // Fire event
        if (drawnCalendarListener == null) {
            throw IllegalStateException("You must assign a valid RobotoCalendarListener first!")
        } else {
            drawnCalendarListener?.onDayClick(calendar)
        }
    }

    private val onDayOfMonthLongClickListener = View.OnLongClickListener { view ->
        // Extract day selected
        val dayOfTheMonthContainer = view as ViewGroup
        var tagId = dayOfTheMonthContainer.tag as String
        tagId = tagId.substring(DAY_OF_THE_MONTH_LAYOUT.length, tagId.length)
        val dayOfTheMonthText = view.findViewWithTag<TextView>(DAY_OF_THE_MONTH_TEXT + tagId)

        // Extract the day from the text
        val calendar = Calendar.getInstance()
        currentCalendar?.get(Calendar.YEAR)?.let { calendar.set(Calendar.YEAR, it) }
        currentCalendar?.get(Calendar.MONTH)?.let { calendar.set(Calendar.MONTH, it) }
        calendar.set(Calendar.DAY_OF_MONTH, Integer.valueOf(dayOfTheMonthText.text.toString())!!)

        // Fire event
        if (drawnCalendarListener == null) {
            throw IllegalStateException("You must assign a valid DrawnCalendarListener first!")
        } else {
            drawnCalendarListener!!.onDayLongClick(calendar)
        }
        true
    }

    // ************************************************************************************************************************************************************************
    // * Getter methods
    // ************************************************************************************************************************************************************************

    private fun getDayOfMonthBackground(currentCalendar: Calendar): ViewGroup {
        return getView(DAY_OF_THE_MONTH_BACKGROUND, currentCalendar) as ViewGroup
    }

    private fun getDayOfMonthText(currentCalendar: Calendar): TextView {
        return getView(DAY_OF_THE_MONTH_TEXT, currentCalendar) as TextView
    }

    fun getCircleImage1(currentCalendar: Calendar): ImageView {
        return getView(DAY_OF_THE_MONTH_CIRCLE_IMAGE_1, currentCalendar) as ImageView
    }

    fun getCircleImage2(currentCalendar: Calendar): ImageView {
        return getView(DAY_OF_THE_MONTH_CIRCLE_IMAGE_2, currentCalendar) as ImageView
    }

    private fun getDayIndexByDate(currentCalendar: Calendar): Int {
        val monthOffset = getMonthOffset(currentCalendar)
        val currentDay = currentCalendar.get(Calendar.DAY_OF_MONTH)
        return currentDay + monthOffset
    }

    private fun getMonthOffset(currentCalendar: Calendar): Int {
        val calendar = Calendar.getInstance()
        calendar.time = currentCalendar.time
        calendar.set(Calendar.DAY_OF_MONTH, 1)
        val firstDayWeekPosition = calendar.firstDayOfWeek
        val dayPosition = calendar.get(Calendar.DAY_OF_WEEK)

        if (firstDayWeekPosition == 1) {
            return dayPosition - 1
        } else {

            if (dayPosition == 1) {
                return 6
            } else {
                return dayPosition - 2
            }
        }
    }

    private fun getWeekIndex(weekIndex: Int, currentCalendar: Calendar?): Int {
        val firstDayWeekPosition = currentCalendar?.firstDayOfWeek

        if (firstDayWeekPosition == 1) {
            return weekIndex
        } else {

            if (weekIndex == 1) {
                return 7
            } else {
                return weekIndex - 1
            }
        }
    }

    private fun getView(key: String, currentCalendar: Calendar) : View {
        val index = getDayIndexByDate(currentCalendar)
        return rootView.findViewWithTag(key + index)
    }


}