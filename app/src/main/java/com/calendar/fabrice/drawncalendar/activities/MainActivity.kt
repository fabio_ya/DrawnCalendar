package com.calendar.fabrice.drawncalendar.activities

import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.calendar.fabrice.drawncalendar.R
import com.calendar.fabrice.drawncalendar.composant.DrawnCalendarView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.drawn_calendar_month_layout.view.*
import java.util.*

class MainActivity : AppCompatActivity(), DrawnCalendarView.DrawnCalendarListener {

    override fun onDayClick(daySelectedCalendar: Calendar) {
        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        if (add_inter_calendar.getCircleImage1(daySelectedCalendar).getVisibility() != View.VISIBLE) {
            add_inter_calendar.clearSelectedDay(daySelectedCalendar, true, true)
            add_inter_calendar.markCircleImage1(daySelectedCalendar)
        } else {
            add_inter_calendar.clearSelectedDay(daySelectedCalendar, true, true)
        }
    }

    override fun onDayLongClick(daySelectedCalendar: Calendar) {
        //  TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onRightButtonClick() {
        // TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onLeftButtonClick() {
        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        add_inter_calendar.setDrawnCalendarListener(this)
        add_inter_calendar.setShortWeekDays(false)
        add_inter_calendar.updateView()
        add_inter_calendar.monthText.setTextColor(Color.GRAY)

    }
}

